/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quizzApp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Admin
 */
public class QuizTest {
    private Quiz quiz;
    private int questionCounter;
    
    public QuizTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        quiz = new Quiz();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testShouldCreateQuestions() {
        quiz.addQuestion("Pytanie 1","odpowiedz 1", "odpowiedz 2", "odpowiedz 3", "odpowiedz 4",3);
        //ostatni parametr wskazuje poprawna odpowiedz
        quiz.addQuestion("Pytanie 2","odpowiedz 1", "odpowiedz 2", "odpowiedz 3", "odpowiedz 4",2);
        quiz.addQuestion("Pytanie 3","odpowiedz 1", "odpowiedz 2", "odpowiedz 3", "odpowiedz 4",1);
        
        questionCounter = quiz.getQuestionsCount();
        
        assertEquals(3,questionCounter);
    }
    
    
    
}
