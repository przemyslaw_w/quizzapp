/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quizzApp;

import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author Admin
 */

@Entity
@Table(name = "Question")
public class Question {
    
    @Id
    @Column(name = "question_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @Column(name = "question_text", nullable = false)
    private String questionText;
    
    @OneToMany
    @JoinColumn(name = "answer_id")
    private Set<Answer> answers;
    
    @ManyToOne
    private Quiz quiz;
    

    public Question(String questionText, Set answers) {
    }

    public Question(String questionText) {
        this.questionText = questionText;
    }

    public Question() {
        
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    void setAnswers(String aswer_1, String answer_2, String answer_3, String answer_4, int correct) {
    }

    public int getAnswerCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean getCorrectAnswer() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
