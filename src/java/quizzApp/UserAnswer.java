/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quizzApp;

import javax.persistence.*;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "UserAnswer")
public class UserAnswer {

    @Id
    @Column(name = "useranswer_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToOne
    @JoinColumn(name = "answer_id", nullable = false)
    private Answer answer;

    public UserAnswer() {
    }
    
    
}
