/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quizzApp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Admin
 */
public class QuestionTest {
    private Question question;
    
    public QuestionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        question = new Question();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getQuestionText method, of class Question.
     */
    @Test
    public void testSet4Answers() {
        question.setAnswers("aswer 1", "answer 2", "answer 3", "answer 4", 3);
        
        assertEquals(4, question.getAnswerCount());
        assertTrue("answer 3", question.getCorrectAnswer());
        
    }

  
}
