/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quizzApp;
import javax.persistence.*;

/**
 *
 * @author Admin
 */

@Entity
@Table(name = "Answer")
public class Answer {
    
    @Id
    @Column(name = "answer_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @Column(name = "answer_text", nullable = false)
    private String answerText;
    
    @Column(name = "is_correct", nullable = false)
    private boolean isCorrect;
    
    @ManyToOne
    private Question question;

    public Answer() {
    }

    public Answer(String aText, boolean t) {
        this.answerText = aText;
        this.isCorrect = t;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public boolean isIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }
    
    
}
